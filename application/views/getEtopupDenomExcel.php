<?php 

// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=export-etopup-denom.xls");
 
// Tambahkan table
?>

<style type="text/css" media="screen">
	tr td {
		border :1px solid #555;
	}
</style>

<table border="1" style="border:1px solid;">
	<tr style="background: #555;color:#FFF;">
		<td>Denomination</td>
		<td>Refil Profile ID</td>
		<td>Validity (Days)</td>
	</tr>
	<?php foreach ($result->result() as $v): ?>
	<tr>
		<td style="text-align: left;" align="left"><?=$v->denomination;?></td>
		<td style="text-align: left;" align="left"><?=$v->refill_profile_id;?></td>
		<td style="text-align: left;" align="left"><?=$v->validity;?></td>
	</tr>
	<?php endforeach ?>
</table>