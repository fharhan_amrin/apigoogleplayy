<?php 

// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=export-internet-denom.xls");
 
// Tambahkan table
?>

<style type="text/css" media="screen">
	tr td {
		border :1px solid #555;
	}
</style>

<table border="1" style="border:1px solid;">
	<tr style="background: #555;color:#FFF;">
		<td>Profilename</td>
		<td>Packagename</td>
		<td>Amount</td>
		<td>Quota</td>
		<td>Validity</td>
		<td>Create Date</td>
	</tr>
	<?php foreach ($result->result() as $v): ?>
	<tr>
		<td style="text-align: left;" align="left"><?=$v->profilename;?></td>
		<td style="text-align: left;" align="left"><?=$v->packagename;?></td>
		<td style="text-align: left;" align="left"><?=$v->amount;?></td>
		<td style="text-align: left;" align="left"><?=$v->quota;?></td>
		<td style="text-align: left;" align="left"><?=$v->validity;?></td>
		<td style="text-align: left;" align="left"><?=$v->created_by;?></td>
	</tr>
	<?php endforeach ?>
</table>