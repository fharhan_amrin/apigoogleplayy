<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter DomPDF Library
 *
 * Generate PDF's from HTML in CodeIgniter
 *
 * @packge        CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author        Ardianta Pargo
 * @license        MIT License
 * @link        https://github.com/ardianta/codeigniter-dompdf
 */
use Dompdf\Dompdf;
class Pdf extends Dompdf{
    /**
     * PDF filename
     * @var String
     */
    public $filename;
    public function __construct(){
        parent::__construct();
        $this->filename = "laporan.pdf";
    }
    /**
     * Get an instance of CodeIgniter
     *
     * @access    protected
     * @return    void
     */
    protected function ci()
    {
        return get_instance();
    }
    /**
     * Load a CodeIgniter view into domPDF
     *
     * @access    public
     * @param    string    $view The view to load
     * @param    array    $data The view data
     * @return    void
     */
    public function load_view($view, $data = array()){
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();
        // Output the generated PDF to Browser
        $output = $this->output();
	// file_put_contents('/home/admin/web/172.17.12.99/public_html/apibonita/uploads/pdf/'.$this->filename, $output);
        file_put_contents('./uploads/pdf/'.$this->filename, $output);
    }
    public function savePDFStock($view, $data = array()){
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();
        // Output the generated PDF to Browser
        $output = $this->output();
        if (file_exists('./uploads/pdf/stock/'.$this->filename)) {
            echo 'ada ./uploads/pdf/stock/'.$this->filename;
            unlink('./uploads/pdf/stock/'.$this->filename);
            file_put_contents('./uploads/pdf/stock/'.$this->filename, $output);
        }else{
            file_put_contents('./uploads/pdf/stock/'.$this->filename, $output);
        }
    }
    public function savePDF($view, $data = array()){
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();
        // Output the generated PDF to Browser
        $output = $this->output();
        if (file_exists('./uploads/pdf/'.$this->filename)) {
            echo 'ada ./uploads/pdf/'.$this->filename;
            unlink('./uploads/pdf/'.$this->filename);
            file_put_contents('./uploads/pdf/'.$this->filename, $output);
        }else{

            file_put_contents('./uploads/pdf/'.$this->filename, $output);
        }
    }















}
