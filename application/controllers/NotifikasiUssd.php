<?php 


defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('DILI/JAKARTA');


class NotifikasiUssd extends CI_Controller {

    public function index()
    {
      $database = $this->load->database('notifikasi',TRUE);
      $this->load->model('TelegramModelUssd', 'tm');

        
       
      $queryData = $database->query('SELECT time , counter , SUM(count) count FROM ussdnode72 where counter!="UMBDATA" AND ussd_node="182"  AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) GROUP BY counter');
      // $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
      $rawData = $queryData->result();
      $success = 0;
      $failed = 0;
      

      foreach ($rawData as $rd) {
        if ($rd->counter == "SUCCESS") {
          $success += $rd->count;
        } else {
          if ($rd->counter == "FAILED_DIALOG_USER_ABORT") {
            $success += $rd->count;
          } else if ($rd->counter == "FAILED_DIALOG_TIMEOUT") {
            $success += $rd->count;
          } else if ($rd->counter == "FAILED_PROVIDER_ABORT") {
            $success += $rd->count;
          } else if ($rd->counter == "FAILED_DIALOG_REJECTED") {
            $success += $rd->count;
          } else {
            $failed += $rd->count;
          }
        }
      }
     
      if ($success != 0) {
        $hasilSukses = $success / ($success + $failed) * 100;
        $data['hasilSukses'] = number_format($hasilSukses, 2);
      } else {
        $data['hasilSukses'] = 0;
      }

      if ($failed != 0) {
        $hasilError = $failed / ($success + $failed) * 100;
        $data['hasilError'] = number_format($hasilError, 2);
      } else {
        $data['hasilError'] = 0;
      }

      // echo json_encode($data);
      $a =  $data['hasilSukses'];
      // echo $a;
      // echo "<br>";
      $b =  $data['hasilError'];
      // echo $b;

      if ($a !=0) {
            if ($a > $b) {
     
          echo "Data success node 182 " .round($a)."% " ." Data failed ".round($b)."%";
         
        }else{
          echo "Data success node 182 " .round($a)."%" ." Data failed ".round($b)."%";
          $nilai= "Data success node 182 " .round($a)."%" ." Data failed ".round($b)."%";
           

           $this->tm->id_pl ='hello';
           $this->tm->task = 'pesan';
           $this->tm->infogpv = $nilai;
           
           $msg = $this->tm->msgToTelegram('u');
          
           $request_params = [
             'chat_id' => "-1001487708334",
             'text' => $msg,
             'parse_mode' => 'HTML'
           ];
           $this->tm->kirimPesan('','',$request_params);


        }
      }else{
        echo "variabel data a sama dengan 0";
      }

      echo "<br>";

       $this->ussdSdt();
       
      
    }


   public function ussdSdt()
  {
      $database = $this->load->database('notifikasi',TRUE);
      $this->load->model('TelegramModelUssd', 'tm');
    $queryDataSdt = $database->query('SELECT time , counter , SUM(count) count FROM ussdnode72 where counter!="UMBDATA" AND ussd_node="187"  AND TIMESTAMP(time) >= TIMESTAMP(NOW() - INTERVAL 15 MINUTE) GROUP BY counter');
      // $queryData = $this->db->query('SELECT FROM_UNIXTIME( ( UNIX_TIMESTAMP(time) DIV (15* 60) ) * (15*60) ) time , counter , SUM(count) count FROM umb.ussdnode72 WHERE time BETWEEN "2019-10-03 21:15:00"  AND "2019-10-03 21:30:00" GROUP BY counter');
      $rawData = $queryDataSdt->result();
      $successSdt = 0;
      $failedSdt = 0;
      // for($i = 0; $i<= sizeof($successRate);$i++){

      foreach ($rawData as $rd) {
        if ($rd->counter == "SUCCESS") {
          $successSdt += $rd->count;
        } else {
          if ($rd->counter == "FAILED_DIALOG_USER_ABORT") {
            $successSdt += $rd->count;
          } else if ($rd->counter == "FAILED_DIALOG_TIMEOUT") {
            $successSdt += $rd->count;
          } else if ($rd->counter == "FAILED_PROVIDER_ABORT") {
            $successSdt += $rd->count;
          } else if ($rd->counter == "FAILED_DIALOG_REJECTED") {
            $successSdt += $rd->count;
          } else {
            $failedSdt += $rd->count;
          }
        }
      }
      // }
      // echo "Success : ".$success."<br/>";
      // echo "Failed : ". $failed;
      if ($successSdt != 0) {
        $hasilSuksesSdt = $successSdt / ($successSdt + $failedSdt) * 100;
        $data['hasilSuksesSdt'] = number_format($hasilSuksesSdt, 2);
      } else {
        $data['hasilSuksesSdt'] = 0;
      }

      if ($failedSdt != 0) {
        $hasilErrorSdt = $failedSdt / ($successSdt + $failedSdt) * 100;
        $data['hasilErrorSdt'] = number_format($hasilErrorSdt, 2);
      } else {
        $data['hasilErrorSdt'] = 0;
      }

      // echo json_encode($data);
      $a =  $data['hasilSuksesSdt'];
      // echo number_format($a, 2);
      // echo "<br>";
      $b =  $data['hasilErrorSdt'];
      // echo $b;

        if ($a != 0) {
            if ($a > $b) {
       
            echo "Data success node 187 " .round($a)."% " ." Data failed ".round($b)."%";
           
        
          }else{
            echo "Data success node 187 " .round($a)."%" ." Data failed ".round($b)."%";
            $nilai = "Data success node 187 " .round($a)."%" ." Data failed ".round($b)."%";
             

             $this->tm->id_pl ='hello';
             $this->tm->task = 'pesan';
             $this->tm->infogpv = $nilai;
             
             $msg = $this->tm->msgToTelegram('d');
            
             $request_params = [
               'chat_id' => "-1001487708334",
               'text' => $msg,
               'parse_mode' => 'HTML'
             ];

            $this->tm->kirimPesan('','',$request_params);

          }
        }else{
          echo "variabel a sama dengan 0";
        }
  }



 public function selesai()
 {
   $data = 0;
   $a = 10;
   if ($data ==0) {
       if ($a > 9) {
             echo "data 1 lebih besar";
       }else{
           echo "data a lebih kecil";
       }
   }else{
       echo "data sama dengan 0";
   }
 }



   
    

}

/* End of file SmsController.php */